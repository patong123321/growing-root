using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class FireCount : MonoBehaviour
{
    [SerializeField] public TMP_Text FireText;

    public static int FireScore = 0;
    // Start is called before the first frame update
    void Start()
    {
        /*FireText.text = "Fire : " + FireScore;*/
    }

    private void Update()
    {
        
        
            FireText.text = "Fire : " + Mathf.Round(FireScore);
        
        
    }

    /*public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            FireScore += 1;
            FireText.text = "Fire : " + FireScore;
            Destroy(other.gameObject);

        }
        
    }
    
    public void OnFire(GameObject actor)
    {
        FireScore += 1;
        FireText.text = "Fire : " + FireScore;

    }*/
}
