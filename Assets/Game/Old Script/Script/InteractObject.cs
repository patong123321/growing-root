using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class InteractObject : MonoBehaviour
{

    public void Open(GameObject actor)
    {
        if (Var.HaveLamp == true)
        {

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 5);
            Timecount1.speed = 1;
            Var.Firecount = 0;
            Var.HaveLamp = false;
            Var.HaveAxe = false;
            Var.HaveLighter = 0;
            Var.HaveShovel = false;
            Var.HaveKey = false;
            Var.HaveKey2 = false;
        }
    }
    public void OpenTemple(GameObject actor)
    {
        if (Var.HaveKey == true)
        {

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 5);
            Timecount1.speed = 1;
            Var.Firecount = 0;
            Var.HaveLamp = false;
            Var.HaveAxe = false;
            Var.HaveLighter = 0;
            Var.HaveShovel = false;
            Var.HaveKey = false;
            Var.HaveKey2 = false;
        }
    }
    
    public void Goal(GameObject actor)
    {
        if (FireCount.FireScore >= 5)
        {
            Var.Firecount --;
            Destroy(gameObject);

            Var.HaveLamp = true;
        }
    }
    
    public void Axe(GameObject actor)
    {
        Destroy(gameObject);
        Var.HaveAxe = true;
        
    }
    
    public void Shovel(GameObject actor)
    {
        Destroy(gameObject);
        Var.HaveShovel = true;
    }
    
    public void DestroyerWood(GameObject actor)
    {
        if (Var.HaveAxe == true)
        {
            Destroy(gameObject);
        }
        
    }
    public void Dig(GameObject actor)
    {
        if (Var.HaveShovel == true)
        {
            Destroy(gameObject);
        }
        
    }
   
    
    public void Chase(GameObject actor)
    { 
        Var.Chase = true;
    }
    
    public void StopChase(GameObject actor)
    {
        Var.Chase = false;

    }
    
    public void B(GameObject actor)
    {
        Destroy(gameObject);
        Timecount1.speed = 3;
        
    }
    
    public void Door(GameObject actor)
    {
        Destroy(gameObject);
        Var.HaveKey2 = true;

    }

    public void Door2(GameObject actor)
    {
        if (Var.HaveKey2 == true)
        {
            Destroy(gameObject);
        }
    }
    
    public void Key(GameObject actor)
    {
        Var.HaveKey = true;
        Destroy(gameObject);

    }
    
    public void HaveKey(GameObject actor)
    {
        if (Var.HaveKey == true)
        {
            Destroy(gameObject);
        }


    }
    
    public void Ending(GameObject actor)
    {
        SceneManager.LoadScene("End");
        
    }
    
    

}
