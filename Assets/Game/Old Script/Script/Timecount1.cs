using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Timecount1 : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI TimerText;
    [SerializeField] private float remainingTime;
    public static int speed = 1;

    // Update is called once per frame
    void Update()
    {
        if (remainingTime > 0)
        {
            remainingTime -= Time.deltaTime*speed;
        }
        

        if (remainingTime < 0)
        {
            remainingTime = 0;
            TimerText.color = Color.red;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            Timecount1.speed = 1;
            Var.Firecount = 0;
            Var.HaveLamp = false;
            Var.HaveAxe = false;
            Var.HaveLighter = 0;
            Var.HaveShovel = false;
            Var.HaveKey = false;
            Var.HaveKey2 = false;

        }
        int minutes = Mathf.FloorToInt(remainingTime / 60);
        int seconds = Mathf.FloorToInt(remainingTime % 60);
        TimerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        /*SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);*/
    }
    
    
}
