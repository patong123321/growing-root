using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Chasing : MonoBehaviour
{
    public NavMeshAgent enemy;
    public Transform player;
    public Transform goal;

    private void Start()
    {
        Var.Chase = false;
    }

    private void Update()
    {
        if (Var.Chase == true)
        {
            enemy.SetDestination(player.position);
            enemy.isStopped = false;
        }
        if (Var.Chase == false)
        {
            NavMeshAgent agent = GetComponent<NavMeshAgent>();
            agent.destination = goal.position; 
            /*enemy.isStopped =true;*/
        }

    }

}
