using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class E : MonoBehaviour
{
    [SerializeField]  GameObject Button;
    

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Interact"))
        {
            Button.gameObject.SetActive(true);
            
        }
        
    }
}
