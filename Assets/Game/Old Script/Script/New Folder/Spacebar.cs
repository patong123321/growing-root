using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spacebar : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
            Timecount1.speed = 1;
            Var.Firecount = 0;
            Var.HaveLamp = false;
            Var.HaveAxe = false;
            Var.HaveLighter = 0;
            Var.HaveShovel = false;
            Var.HaveKey = false;
            Var.HaveKey2 = false;
        }
        
        
    }
}
