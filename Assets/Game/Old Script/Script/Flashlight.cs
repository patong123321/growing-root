using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour
{
    [SerializeField] Vector3 vectoroffset;
    [SerializeField] GameObject goFollow;
    [SerializeField] float speed = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        vectoroffset = transform.position - goFollow.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = goFollow.transform.position + vectoroffset;
        transform.rotation = Quaternion.Slerp(transform.rotation,goFollow.transform.rotation,speed*Time.deltaTime);
    }
}
