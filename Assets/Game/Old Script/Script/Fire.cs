using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public bool CanFire;
    // Start is called before the first frame update
    void Start()
    {
        CanFire = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void OnFire(GameObject actor)
    {
        if (CanFire == true)
        {
            FireCount.FireScore += 1;
            CanFire = false;
        }
        else
        {
            FireCount.FireScore += 0;
        }
        
    }
}
