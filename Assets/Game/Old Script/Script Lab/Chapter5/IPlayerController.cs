using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Patipat.GameDev3.Chapter5.PlayerController
{
    public interface IPlayerController
    {
        void MoveForward();
        void MoveForwardSprint();
        
        void MoveBackward();
        
        void TurnLeft();
        void TurnRight();
    }
}