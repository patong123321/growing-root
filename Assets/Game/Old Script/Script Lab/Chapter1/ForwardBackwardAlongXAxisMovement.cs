using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Patipat.GameDev3.Chapter1
{
    public class ForwardBackwardAlongXAxisMovement : MonoBehaviour
    {
        public const float MAX_MOVEMENT_DISTANCE = 2.0f;
        float m_DisplacementCounter = 0;
        
        [SerializeField]
        private float m_XComponentSpeed = 0.02f;
        Vector3 m_MovementSpeed = Vector3.zero;
        
        // Use this for initialization
        void Start () 
        {
            m_MovementSpeed.x = m_XComponentSpeed;
        }
        
        // Update is called once per frame
        void Update()
        {
            this.transform.position += m_MovementSpeed;
            m_DisplacementCounter += m_MovementSpeed.x;
            
            if (Mathf.Abs(m_DisplacementCounter) > MAX_MOVEMENT_DISTANCE) 
            {
                m_DisplacementCounter = 0;
                m_MovementSpeed *= -1;
            }
        }
    }
}