using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Patipat.GameDev3.Chapter1;
using Patipat.GameDev3.Chapter5.InteractionSystem;
using Patipat.GameDev3.Chapter6.InventorySystem;

namespace Patipat.GameDev3.Chapter6.InteractionSystem
{
    [RequireComponent(typeof(ItemTypeComponent))]
    public class Pickupable : MonoBehaviour, IInteractable , IActorEnterExitHandler
    {
        public void Interact(GameObject actor)
        {
            //myself
            var itemTypeComponent = GetComponent <ItemTypeComponent >();
            //actor
            var inventory = actor.GetComponent <IInventory >();
            inventory.AddItem(itemTypeComponent.Type.ToString(),1);
        
            Destroy(gameObject);
        }

        public void ActorEnter(GameObject actor)
        {
    
        }

        public void ActorExit(GameObject actor)
        {
    
        }
    }
}