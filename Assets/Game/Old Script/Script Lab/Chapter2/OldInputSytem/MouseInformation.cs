using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Patipat.GameDev3.Chapter2
{
    public class MouseInformation : MonoBehaviour
    {
        // Start is called before the first frame update
        public Text m_TextMousePosition;
        public Text m_TextMouseScrollDelta;
        public Text m_TextMouseDeltaVector;
        private Vector3 m_MousePreviousPosition;
// Update is called once per frame
        void Update () 
        {
            Vector3 mouseCurrentPos = Input.mousePosition;
            Vector3 mouseDeltaVector = Vector3.zero;
            mouseDeltaVector = (mouseCurrentPos - m_MousePreviousPosition).normalized;
            m_TextMousePosition.text = Input.mousePosition.ToString();
            m_TextMouseScrollDelta.text = Input.mouseScrollDelta.ToString();
            m_TextMouseDeltaVector.text = mouseDeltaVector.ToString();
            m_MousePreviousPosition = mouseCurrentPos;
        }
    }
}