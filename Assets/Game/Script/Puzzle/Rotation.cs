using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    private Transform _transform;
    public float rotationAmount = 90f;

    public bool interact;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Moving(GameObject gameObject)
    {
        
        /*_transform.rotation = Quaternion.Euler(0f, 90f, 0f);*/
        interact = true;
        transform.Rotate(Vector3.up, rotationAmount);
    }
    
}
