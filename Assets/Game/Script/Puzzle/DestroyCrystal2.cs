using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCrystal2 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Crystal3.OnCrystal3 == true && Crystal4.OnCrystal4 == true && Crystal5.OnCrystal5 == true)
        {
            Invoke("resetOnCrystal",1f);
            Destroy(gameObject);
            //Sound
            SoundManager.instance.crystalSource.PlayOneShot(SoundManager.instance.crystalSound);
        }
    }

    public void resetOnCrystal()
    {
        Crystal3.OnCrystal3 = false;
        Crystal4.OnCrystal4 = false;
        Crystal5.OnCrystal5 = false;
        
    }
}
