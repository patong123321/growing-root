using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movepiece : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mouseposition = new Vector3(Input.mousePosition.x, Input.mousePosition.y,Input.mousePosition.z);
        Vector3 objposition = Camera.main.ScreenToWorldPoint(mouseposition);
        transform.position = objposition;
    }
}
