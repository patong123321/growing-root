using System.Collections;
using System.Collections.Generic;
using HealthSystem;
using UnityEngine;

public class Laser : MonoBehaviour
{
    private LineRenderer lineLaser;
    public int DamageToGive = 1;
    void Start () {
        lineLaser = GetComponent<LineRenderer>();
    }
 
    // Update is called once per frame
    void Update () {
        lineLaser.SetPosition(0, transform.position);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {
            if (hit.collider)
            {
                lineLaser.SetPosition(1, hit.point);
                
                if (hit.collider.CompareTag("Player"))
                {
                    lineLaser.SetPosition(1, hit.point);
                    FindObjectOfType<HealthManager>().DamagePlayer(DamageToGive);
            
                    SoundManager.instance.DamageSource.PlayOneShot(SoundManager.instance.Damagesound);
                }
            }
            

            
        }
        else lineLaser.SetPosition(1, transform.forward*10);
    }

    
}
