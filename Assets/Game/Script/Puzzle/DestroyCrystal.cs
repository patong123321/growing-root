using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCrystal : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Crystal1.OnCrystal1 == true && Crystal2.OnCrystal2 == true)
        {
            Invoke("resetOnCrystal",1f);
            Destroy(gameObject);
            //Sound
            SoundManager.instance.crystalSource.PlayOneShot(SoundManager.instance.crystalSound);
        }
    }

    public void resetOnCrystal()
    {
        Crystal1.OnCrystal1 = false;
        Crystal2.OnCrystal2 = false;
    }
}
