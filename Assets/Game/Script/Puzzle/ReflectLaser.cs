using System.Collections;
using System.Collections.Generic;
using HealthSystem;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class ReflectLaser : MonoBehaviour
{
    public LayerMask layerMask;
    public LayerMask layerMask2;
    public float defaultLenght = 50;
    public int numberOfReflection = 2;

    private LineRenderer laser;
    private RaycastHit hit;

    private Ray ray;
    private Vector3 directions;

    [SerializeField]
    public bool test;
    public int damageToGive = 1;
    
    public static bool Crystal1;
    public static bool Crystal2;
    
    public static bool Crystal3;
    public static bool Crystal4;
    public static bool Crystal5;
    
    // Start is called before the first frame update
    void Start()
    {
        laser = GetComponent<LineRenderer>();
        Crystal1 = false;
        Crystal2 = false;

    }

    // Update is called once per frame
    void Update()
    {
        Reflectionlasrt();
    }

    public void Reflectionlasrt()
    {
        ray = new Ray(transform.position, transform.forward);
        laser.positionCount = 1;
        laser.SetPosition(0, transform.position);

        float remainLenght = defaultLenght;

        for (int i = 0; i < numberOfReflection; i++)
        {
            if (Physics.Raycast(ray.origin, ray.direction, out hit, remainLenght, layerMask))
            {
                laser.positionCount += 1;
                laser.SetPosition(laser.positionCount -1 , hit.point);
                remainLenght -= Vector3.Distance(ray.origin, hit.point);
                ray = new Ray(hit.point, Vector3.Reflect(ray.direction, hit.normal));
                if (hit.collider.CompareTag("Crystal1"))
                {
                    Crystal1 = true;
                    
                }
                
                if (hit.collider.CompareTag("Crystal2"))
                {
                    Crystal2 = true;
                }
                
                if (hit.collider.CompareTag("Crystal3"))
                {
                    Crystal3 = true;
                }
                    
                if (hit.collider.CompareTag("Crystal4"))
                {
                    Crystal4 = true;
                }
                
                if (hit.collider.CompareTag("Crystal5"))
                {
                    Crystal5 = true;
                }
            }
            else
            {
                laser.positionCount += 1;
                laser.SetPosition(laser.positionCount -1,ray.origin + (ray.direction * remainLenght));
            }
            
            

        }
    }
}
