using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Push : MonoBehaviour
{
    public Rigidbody rb;
    public float pushForce = 5f;
    /*private GameObject wall;*/

    public static bool onWall;

    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if(onWall == true){}
        if (other.CompareTag("Stick"))
        {
            rb.AddForce(player.transform.forward * pushForce, ForceMode.Impulse);
            if (Input.GetKeyDown(KeyCode.F))
            {
                rb.AddForce(player.transform.forward * pushForce, ForceMode.Impulse);
            }
        }
    }

    /*public void PushObject()
    {
        StartCoroutine(PushObjectOverTime());
    }

    public void StopPush()
    {
        if (rb.velocity.magnitude > 0) // ถ้า object ยังมีความเร็ว
        {
            rb.velocity = Vector3.zero; // หยุดความเร็ว
            rb.angularVelocity = Vector3.zero; // หยุดการหมุน
        }
        pushForce = 0;
    }*/
    
    /*IEnumerator PushObjectOverTime()
    {
        float timer = 0f;
        while (timer < duration)
        {
            timer += Time.deltaTime;
            Vector3 pushDirection = transform.forward * pushForce;
            rb.AddForce(pushDirection, ForceMode.Impulse);
            yield return null;
        }
    */
    /*}*/
}
