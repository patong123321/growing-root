using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Information : MonoBehaviour
{
    public Transform player;
    public GameObject infoUI;
    [SerializeField] public float radiusInfo;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(player.position, transform.position);
        if (distance <=radiusInfo)
        {
            infoUI.SetActive(true);
        }

        if (distance > radiusInfo)
        {
            infoUI.SetActive(false);
        }
    }
    
    void OnDrawGizmosSelected ()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radiusInfo);

    }
}
