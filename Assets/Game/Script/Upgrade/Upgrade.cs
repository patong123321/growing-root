using System;
using System.Collections;
using System.Collections.Generic;
using HealthSystem;
using StarterAssets;
using UnityEngine;
using TMPro;

public class Upgrade : MonoBehaviour
{
    public bool haveDoubleJump;
    public bool haveDash;
    public bool haveHighJump;
    public static bool AddMaxHp = false;

    public static bool addHealingBottle;
    public static bool maxBottle;
    public bool haveHealBottle;
        
    public int costDoubleJump = -1;
    public int costDash = -2;
    public int costHighJump = -3;
    public int cost2 = -10;
    public GameObject iconDoubleJump;
    public GameObject iconDash;
    public GameObject iconHighJump;
    public GameObject iconHealBottle;

    public void Start()
    {
        haveDoubleJump = false;
        haveDash = false;
        haveHighJump = false;
    }

    private void Update()
    {
        if (haveDoubleJump == true)
        {
            iconDoubleJump.SetActive(true);
        }
        if (haveDash == true)
        { 
            iconDash.SetActive(true);
        }
        if (haveHighJump == true)
        {
            iconHighJump.SetActive(true);
        }
        if (haveHealBottle == true)
        {
            iconHealBottle.SetActive(true);
        }
    }
        

    public void DoubleJumpSkill()
    {
        if (Gamemanager.currentSunEssence > 0 && haveDoubleJump == false)
        {
            ThirdPersonController.GetDoubleJump = true;
            FindObjectOfType<Gamemanager>().AddSunEssence(costDoubleJump);
            haveDoubleJump = true;
        }
        
    }
        
    public void DashSkill()
    {
        if (Gamemanager.currentSunEssence >= 2 && haveDash == false)
        {
            ThirdPersonController.GetDash = true;
            Debug.Log("haveDash");
            FindObjectOfType<Gamemanager>().AddSunEssence(costDash);
            haveDash = true;
        }
        
    }
        
    public void HighJumpSkill()
    {
        if (Gamemanager.currentSunEssence >= 3 && haveHighJump == false)
        {
            ThirdPersonController.GetHighJump = true;
            Debug.Log("haveHihJump");
            FindObjectOfType<Gamemanager>().AddSunEssence(costHighJump);
            haveHighJump = true;
        }
        
    }

    public void MaxHpUpgrade()
    {
        if (Gamemanager.currentEssence >= 10 && AddMaxHp == false)
        {
            FindObjectOfType<Gamemanager>().AddEssence(cost2);
            AddMaxHp = true;
        }
    }
    public void HealingBottle()
    {
        if (Gamemanager.currentEssence >= 10 && addHealingBottle == false && maxBottle == false)
        {
            FindObjectOfType<Gamemanager>().AddEssence(cost2);
            addHealingBottle = true;
            haveHealBottle = true;
        }
        
    }
}