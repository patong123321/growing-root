using System.Collections;
using System.Collections.Generic;
using HealthSystem;
using UnityEngine;
using TMPro;

public class Healing : MonoBehaviour
{
    public static int maxHealBottle = 0;
    public static int currentHealBottle;

    public int heal = 20;

    public GameObject healEffect;

    public TMP_Text currentHealBottleText;
    // Start is called before the first frame update
    void Start()
    {
        currentHealBottle = maxHealBottle;
        maxHealBottle = 0;
    }

    // Update is called once per frame
    void Update()
    {
        UseHealing();
        currentHealBottleText.text = "" + currentHealBottle;
        if (Upgrade.addHealingBottle == true && Upgrade.maxBottle == false)
        {
            maxHealBottle += 1;
            currentHealBottle += 1;
            Upgrade.addHealingBottle = false;
        }
        if (maxHealBottle == 5)
        {
            maxHealBottle = 5;
            Upgrade.maxBottle = true;
        }
        
    }

    public void UseHealing()
    {
        if (maxHealBottle > 0)
        {
            if (Input.GetKeyDown(KeyCode.R) && currentHealBottle != 0)
            {
                SoundManager.instance.healSource.PlayOneShot(SoundManager.instance.healSound);
                currentHealBottle -= 1;
                FindObjectOfType<HealthManager>().HealPlayer(heal);
                
                healEffect.SetActive(true);
                Invoke("EffectSetActiveFalse",2f);
            }
        }
    }

    public void ResetBottle()
    {
        currentHealBottle = maxHealBottle;
    }

    public void EffectSetActiveFalse()
    {
        healEffect.SetActive(false);
    }
    
}
