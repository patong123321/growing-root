using System.Collections;
using System.Collections.Generic;
using StarterAssets;
using UnityEngine;

public class Shop : MonoBehaviour
    { 
        public GameObject ShopMenu;
        private bool menuActivated;
        // Start is called before the first frame update
        void Start()
        {
            ShopMenu.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            //check
            if (Input.GetKeyDown(KeyCode.I) && menuActivated)
            {
                Time.timeScale = 1;
                ShopMenu.SetActive(false);
                menuActivated = false;
                Cursor.lockState = CursorLockMode.Locked;
                StarterAssetsInputs.cursorInputForLook = true;


            }
            else if(Input.GetKeyDown(KeyCode.I)&&!menuActivated)
            {
                Time.timeScale = 0;
                ShopMenu.SetActive(true);
                menuActivated = true;
                Cursor.lockState = CursorLockMode.None;
                StarterAssetsInputs.cursorInputForLook = false;
                

            }
        }

        public void ActivateMune()
        {
            if (menuActivated)
            {
                Time.timeScale = 1;
                ShopMenu.SetActive(false);
                menuActivated = false;
                Cursor.lockState = CursorLockMode.Locked;
                StarterAssetsInputs.cursorInputForLook = true;
                Attack.hasAttacked = false;
            }
            else if(!menuActivated)
            {
                Time.timeScale = 0;
                ShopMenu.SetActive(true);
                menuActivated = true;
                Cursor.lockState = CursorLockMode.None;
                StarterAssetsInputs.cursorInputForLook = false;
                Attack.hasAttacked = true;
            }
        }
    
    }