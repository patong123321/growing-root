using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Cooldown : MonoBehaviour
{
    [SerializeField] TMP_Text cooldownText;
    [SerializeField] private float remainingTime;

    public GameObject dashCooldown;
    public static bool DashHasCooldown;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (DashHasCooldown == true)
        {
            remainingTime = 3;
            DashHasCooldown = false;
            dashCooldown.SetActive(true);
        }
        if (remainingTime > 0)
        {
            remainingTime -= Time.deltaTime;
        }
        

        if (remainingTime < 0)
        {
            dashCooldown.SetActive(false);
            DashHasCooldown = false;
            remainingTime = 0;
            cooldownText.color = Color.red;

        }
        int minus = Mathf.FloorToInt(remainingTime / 60);
        int seconds = Mathf.FloorToInt(remainingTime % 60);
        cooldownText.text = string.Format("{0:00}", seconds);
    }
}
