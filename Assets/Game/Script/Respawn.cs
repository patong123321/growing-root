using System.Collections;
using System.Collections.Generic;
using StarterAssets;
using UnityEngine;
using UnityEngine.Serialization;

public class Respawn : MonoBehaviour
{
    [SerializeField]
    private Transform player;
    [FormerlySerializedAs("Resapwnpoint")] [SerializeField]
    private Transform Respawnpoint;

    public bool isDead = false;
    
    void Start()
    {
        
    }
    
    void Update()
    {
        /*StartCoroutine(respawnCoroutine());*/
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Invoke("Delayed",0f);
        }
    }
    void Delayed()
    {
        player.transform.position = Respawnpoint.transform.position;
        Physics.SyncTransforms();
        
    }
    
    
}
