using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scale0 : MonoBehaviour
{
    public GameObject gameobject;
    public GameObject gameobject2;

    public bool open;
    // Start is called before the first frame update
    void Start()
    {
        open = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void scale0()
    {
        StartCoroutine(ScaleOverTime(Vector3.zero, 3.0f));
        StartCoroutine(ScaleOverTime1(Vector3.zero, 3.0f));
        /*if (open == false)
        {
            
            open = true;
            /*StartCoroutine(ScaleOverTime1(Vector3.one, 3.0f));#1#
        }

        if (open == true)
        {
            /*StartCoroutine(ScaleOverTime1(Vector3.zero, 3.0f));#1#
            open = false;
            StartCoroutine(ScaleOverTime(Vector3.one, 3.0f));
        }*/

    }
    
    IEnumerator ScaleOverTime(Vector3 scale, float time)
    {
        float elapsedTime = 0f;
        Vector3 startingScale = gameobject.transform.localScale;

        while (elapsedTime < time)
        {
            gameobject.transform.localScale = Vector3.Lerp(startingScale, scale, elapsedTime / time);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        // ครบเวลาแล้วกำหนด Scale ให้เป็นค่าสุดท้าย (เพื่อป้องกันความคลาดเคลื่อน)
        gameobject.transform.localScale = scale;
    }
    
    IEnumerator ScaleOverTime1(Vector3 scale, float time)
    {
        float elapsedTime = 0f;
        Vector3 startingScale = gameobject2.transform.localScale;

        while (elapsedTime < time)
        {
            gameobject2.transform.localScale = Vector3.Lerp(startingScale, scale, elapsedTime / time);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        // ครบเวลาแล้วกำหนด Scale ให้เป็นค่าสุดท้าย (เพื่อป้องกันความคลาดเคลื่อน)
        gameobject2.transform.localScale = scale;
    }
}
