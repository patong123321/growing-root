using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    [SerializeField]
    private Transform player;
    [SerializeField]
    public Transform teleportTarget;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Invoke("Delayed",1f);
        }
    }

    void Delayed()
    {
        player.transform.position = teleportTarget.transform.position;
        Physics.SyncTransforms();
        Debug.Log("on");
    }
}

