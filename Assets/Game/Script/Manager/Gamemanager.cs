using System.Collections;
using System.Collections.Generic;
using StarterAssets;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class Gamemanager : MonoBehaviour
{
    public static int currentSunEssence;
    public TMP_Text SunEs_Text;
    
    public static int currentEssence;
    public TMP_Text Es_Text;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        StarterAssetsInputs.cursorInputForLook = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddSunEssence(int SunEsToAdd)
    {
        currentSunEssence += SunEsToAdd;
        SunEs_Text.text = "SunEssence: " + currentSunEssence;
    }
    
    public void AddEssence(int EsToAdd)
    {
        currentEssence += EsToAdd;
        Es_Text.text = "Essence: " + currentEssence;
    }
    
    
}
