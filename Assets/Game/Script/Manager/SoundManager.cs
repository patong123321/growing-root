using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;
    public AudioSource EssenceSource;
    public AudioClip Essencesound;
    
    public AudioSource SunEssenceSource;
    public AudioClip SunEssencesound;

    public AudioSource DamageSource;
    public AudioClip Damagesound;

    public AudioSource AttackSource;
    public AudioClip Attacksound;
    
    public AudioSource crystalSource;
    public AudioClip crystalSound;
    
    public AudioSource crystalSource2;
    public AudioClip crystalSound2;
    
    public AudioSource wallSource;
    public AudioClip wallSound;
    
    public AudioSource rotateSource;
    public AudioClip rotateSound;
    
    public AudioSource healSource;
    public AudioClip healSound;
    
    public AudioSource dialogueSource;
    public AudioClip  dialogueSound;

    public AudioSource jumpSource;
    public AudioClip  jumpSound;
    
    public AudioSource chargeSource;
    public AudioClip  chargeSound;
    public AudioSource chargeSource2;
    public AudioClip  chargeSound2;
    
    public AudioSource dashSource;
    public AudioClip  dashSound;
    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
