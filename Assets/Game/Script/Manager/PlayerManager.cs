using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    #region Singleton

    public static PlayerManager instrance;

    private void Awake()
    {
        instrance = this;
    }

    #endregion
    public GameObject player;
}
