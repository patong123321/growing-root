using System;
using System.Collections;
using System.Collections.Generic;
using StarterAssets;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace HealthSystem
{
    public class HealthManager : MonoBehaviour
    {
        public static float currentHealth ;
        public float maxHealth = 100;
        public Slider healthBar;
        /*[SerializeField] public TMP_Text HealthText;*/

        public static bool resetText;
        
        [SerializeField]
        private Transform player;
        [SerializeField]
        private Transform respawnPoint;
        
        Animator animator;
        int _isDeath;
        int _isHurt;

        // Start is called before the first frame update
        void Start()
        {
            currentHealth = maxHealth;
            animator = GetComponent<Animator>();
            _isHurt = Animator.StringToHash("Hurt");
            _isDeath = Animator.StringToHash("Death");

        }

        // Update is called once per frame
        void Update()
        {
            if (healthBar.value != currentHealth)
            {
                healthBar.value = currentHealth;
            }
            if (healthBar.value == 0)
            {
                healthBar.value = 0; 
                player.transform.position = respawnPoint.transform.position;
                Physics.SyncTransforms();
            }
            
            /*if (Upgrade.AddMaxHp == true)
            {
                maxHealth += 10;
                Debug.Log("CheckHP");
                Debug.Log(currentHealth);
                Debug.Log(maxHealth);

                Upgrade.AddMaxHp = false;
            }*/

            if (Checkhp.HealHP == true)
            {
                currentHealth = maxHealth;
                Checkhp.HealHP = false;
                Debug.Log(Checkhp.HealHP);
                Debug.Log(currentHealth);
            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                currentHealth = 0;
            }
        }

        public void DamagePlayer(int damage)
        {
            currentHealth -= damage;
            animator.SetTrigger(_isHurt);
            SoundManager.instance.DamageSource.PlayOneShot(SoundManager.instance.Damagesound);
            if (currentHealth <= 0)
            {
                Death();
            }
        }

        public void Death()
        {
            animator.SetBool(_isDeath,true);
            currentHealth = maxHealth;
            player.transform.position = respawnPoint.transform.position;
            Physics.SyncTransforms();
            Cursor.lockState = CursorLockMode.Locked;
            StarterAssetsInputs.cursorInputForLook = true;
        }

        public void HealPlayer(int healAmount)
        {
            currentHealth += healAmount;
            if (currentHealth > maxHealth)
            {
                currentHealth = maxHealth;
            }
        }

        public void CheckmaxHP()
        {
            currentHealth = maxHealth;
            Debug.Log("CheckHP");
            Debug.Log(currentHealth);
            Debug.Log(maxHealth);
        }
    }
}