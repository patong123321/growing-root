using System;
using System.Collections;
using System.Collections.Generic;
using StarterAssets;
using UnityEngine;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    public TMP_Text nameText;
    public TMP_Text diaologueText;
    
    private Queue<string> sentences;
    
    public GameObject dialogueUI;

    [SerializeField]
    public float textSpeed = 0;

    public GameObject questUI;

    public static bool dialogueActive;

    public GameObject forestPortal;
    

    private void Start()
    {
        sentences = new Queue<string>(1);
        dialogueActive = false;
        forestPortal.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && dialogueActive == true)
        {
            DisplayNextSentecne();
            SoundManager.instance.dialogueSource.PlayOneShot(SoundManager.instance.dialogueSound);
        }
        
    }

    public void StartDialogue(Dialogue dialogue)
    {
        Debug.Log("Start conversation with "+ dialogue.name);
        dialogueActive = true;
        dialogueUI.SetActive(true);
        
        Cursor.lockState = CursorLockMode.Locked;
        StarterAssetsInputs.cursorInputForLook = false;
        
        nameText.text = dialogue.name;
        
        sentences.Clear();

        foreach (string sentence in dialogue.sentence)
        {
            sentences.Enqueue(sentence);
        }
    }

    public void DisplayNextSentecne()
    {
        
        if (sentences.Count == 0 )
        {
            EndDialogue();
            return;
        }
        string sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));

    }

    IEnumerator TypeSentence (string sentence)
    {
        diaologueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            diaologueText.text += letter;
            yield return new WaitForSeconds(textSpeed);
        }
    }

    private void EndDialogue()
    {
        Debug.Log("End of Conversation.");
        dialogueUI.SetActive(false);
        questUI.SetActive(true);
        Cursor.lockState = CursorLockMode.Locked;
        StarterAssetsInputs.cursorInputForLook = true;
        forestPortal.SetActive(true);
    }
}
