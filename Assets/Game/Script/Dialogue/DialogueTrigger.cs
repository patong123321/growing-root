using System;
using System.Collections;
using System.Collections.Generic;
using StarterAssets;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public Dialogue dialogue;

    public GameObject dialogueUI;

    public bool dialogueActive;

    public void Update()
    {

        
    }

    public void TriggerDialogue()
    {
       
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
        

        /*Attack.hasAttacked = false;*/
    }
}
