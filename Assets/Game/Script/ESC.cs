using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StarterAssets;

public class ESC : MonoBehaviour
{
    public GameObject escMenu;
    private bool EscActivated;
    // Start is called before the first frame update
    void Start()
    {
        escMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && EscActivated)
        {
            Time.timeScale = 1;
            escMenu.SetActive(false);
            EscActivated = false;
            Cursor.lockState = CursorLockMode.Locked;
            StarterAssetsInputs.cursorInputForLook = true;
            Attack.hasAttacked = false;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && !EscActivated)
        {
            Time.timeScale = 0;
            escMenu.SetActive(true);
            EscActivated = true;
            Cursor.lockState = CursorLockMode.None;
            StarterAssetsInputs.cursorInputForLook = false;
            Attack.hasAttacked = true;
        }
    }
    
}
