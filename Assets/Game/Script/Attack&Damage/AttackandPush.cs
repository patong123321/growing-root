using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackandPush : MonoBehaviour
{
    Animator animator;
    int isAttackHash;

    public static bool hasAttacked;
    private float timeSinceAttack;
    public int currentAttack = 0;
    
    //BoxCollider
    public BoxCollider boxCollider;
    public static bool attackActivated;
    
    //effect
    public GameObject effect;
    public GameObject effectPos;
    
    // Start is called before the first frame update
    void Start()
    {

        hasAttacked = false;
        animator = GetComponent<Animator>();
        isAttackHash = Animator.StringToHash("isAttack");
        
    }

    // Update is called once per frame
    void Update()
    {
        timeSinceAttack += Time.deltaTime;
        CanAttack();
        if (attackActivated == true)
        {
            Invoke("Removebox", 1f);
            attackActivated = false;
        }
        
    }

    private void CanAttack()
    {
        if (hasAttacked == false)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                //Colider
                boxCollider.enabled = true;
                attackActivated = true;

                //Attack
                animator.SetTrigger(isAttackHash);
                hasAttacked = true;

                Invoke("CooldownAttack", 2f);
                
                //Sound
                SoundManager.instance.AttackSource.PlayOneShot(SoundManager.instance.Attacksound);
                //Effect
                Invoke("Effect",0.625f);
            }
        }
    }

    void Removebox()
    {
        boxCollider.enabled = false;
        Debug.Log("Box Collider is out ");
    }

    /*void Effect()
    {
        Instantiate(effect,effectPos.transform.position,effectPos.transform.rotation);
    }*/
    
    void CooldownAttack()
    {
        hasAttacked = false;
    }
}

