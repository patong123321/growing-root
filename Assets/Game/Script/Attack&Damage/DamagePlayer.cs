using System;
using System.Collections;
using System.Collections.Generic;
using HealthSystem;
using UnityEngine;

public class DamagePlayer : MonoBehaviour
{
    public int DamageToGive = 10;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            /*Destroy(transform.GetComponent<Rigidbody>());
            other.GetComponent<HealthManager>().DamagePlayer(damage:10);*/
            FindObjectOfType<HealthManager>().DamagePlayer(DamageToGive);
            
            SoundManager.instance.DamageSource.PlayOneShot(SoundManager.instance.Damagesound);
        }
        
    }
}
