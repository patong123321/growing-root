using System;
using System.Collections;
using System.Collections.Generic;
using HealthSystem;
using UnityEngine;

public class Damage1 : MonoBehaviour
{
    /*public int DamageToGive = 10;*/
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("AI"))
        {
            Destroy(transform.GetComponent<Rigidbody>());
            other.GetComponent<AIHealth>().TakeDamageAI(20);
            /*FindObjectOfType<AIHealth>().TakeDamage(damage: 10);*/
        }
        
        
    }
}
