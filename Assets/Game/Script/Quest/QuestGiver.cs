using System;
using System.Collections;
using System.Collections.Generic;
using HealthSystem;
using StarterAssets;
using UnityEngine;
using TMPro;

public class QuestGiver : MonoBehaviour
{
    public Quest quest;
    public ThirdPersonController player;

    public GameObject questWindow;
    public TMP_Text titleText;
    public TMP_Text description;
    
    public int nextQuest;

    public static bool completeSeed;
    public static bool completeWater;
    public static bool completeFertilizer;

    private void Start()
    {
        /*questWindow.SetActive(true);*/
        titleText.text = quest.title;
        description.text = quest.descriptionSeed;
        completeSeed = false;
        completeWater = false;
        
    }

    private void Update()
    {
        if (HealthManager.resetText == true)
        {
            description.text = quest.descriptionSeed;
        }
        if (CollectSeed.GetSeed == true)
        {
            description.text = quest.descriptionSeed2;
            
        }
        if (completeSeed == true)
        {
            description.text = quest.descriptionWater;
        }
        
        if (CollectWater.GetWater == true && completeSeed == true)
        {
            description.text = quest.descriptionWater2;
        }

        

        if (completeWater == true)
        {
            description.text = quest.descriptionFertilizer;
        }
        
        if (CollectFertilizer.GetFertilizer == true && completeWater == true && completeSeed == true)
        {
            description.text = quest.descriptionFertilizer2;
        }
        
        if (completeFertilizer == true)
        {
            description.text = quest.descriptionEnd;
        }

        
        
    }

    public void OpenQuestWindow()
    {
        /*questWindow.SetActive(true);
        titleText.text = quest.title;
        description.text = quest.description_Seed;
        if (completeSeed == true)
        {
            description.text = quest.descriptionWater;
        }*/


    }

}
