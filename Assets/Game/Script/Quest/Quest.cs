using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Quest : MonoBehaviour
{
    public bool isActive;
    
    public string title;
    public string descriptionSeed;
    public string descriptionSeed2;
    public string descriptionWater;
    public string descriptionWater2;
    public string descriptionFertilizer;
    public string descriptionFertilizer2;
    public string descriptionEnd;
    public bool reward;
}
