using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crystal5 : MonoBehaviour
{
    public static bool OnCrystal5;
    public Material newMaterial;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (ReflectLaser.Crystal5 == true)
        {
            GetComponent<Renderer>().material = newMaterial;
            OnCrystal5 = true;
        }
    }
}
