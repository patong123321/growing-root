using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crystal2 : MonoBehaviour
{
    public static bool OnCrystal2;
    public Material newMaterial;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (ReflectLaser.Crystal2 == true)
        {
            GetComponent<Renderer>().material = newMaterial;
            OnCrystal2 = true;
        }

    }
    
}
