using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crystal4 : MonoBehaviour
{
    public static bool OnCrystal4;
    public Material newMaterial;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (ReflectLaser.Crystal4 == true)
        {
            GetComponent<Renderer>().material = newMaterial;
            OnCrystal4 = true;
        }
    }
}
