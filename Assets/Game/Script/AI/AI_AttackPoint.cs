using System.Collections;
using System.Collections.Generic;
using HealthSystem;
using UnityEngine;

public class AI_AttackPoint : MonoBehaviour
{
    public int attackDamage = 20;
    /*private float attactRate = 2f;
    private float nextAttackTime = 0f;*/
    public LayerMask playerLayers;
    
    public Transform attackPoint;
    public float attackRange = 0.5f;
    
    // Start is called before the first frame update

    public void Attack()
    {
        Collider[] hitPlayer = Physics.OverlapSphere(attackPoint.position, attackRange, playerLayers);
        foreach (Collider player in hitPlayer) 
        { 
            FindObjectOfType<HealthManager>().DamagePlayer(attackDamage);
        }
    } 
    
    void OnDrawGizmosSelected ()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }
}
