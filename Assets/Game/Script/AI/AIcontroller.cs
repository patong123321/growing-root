using System;
using System.Collections;
using System.Collections.Generic;
using HealthSystem;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;

public class AIcontroller : MonoBehaviour
{
    public float lookRadius = 10f;
    
    public float attackRadius = 3f;

    Transform target;
    NavMeshAgent agent;
    
    Animator animator;
    int isWalkingHash;
    int _isAttackHash;
    
    /*[Serialize] public static bool Attacked;
    
    private float attackCountdown = 0f;
    
    //BoxCollider
    public BoxCollider boxCollider;
    public bool attackActivated;
    
    [FormerlySerializedAs("resetattack")] [SerializeField] 
    public float timeToBoxtrue = 1f;*/
    
    public List<Transform> waypoint;
    public int currentWaypointIndex = 0;

    /*
    public int attackDamage = 20;
    private float attactRate = 2f;
    private float nextAttackTime = 0f;
    public LayerMask playerLayers;
    
    public Transform attackPoint;
    public float attackRange = 0.5f;
    public bool Attacked;
    */
    

    void Start()
    {
        target = PlayerManager.instrance.player.transform;
        agent = GetComponent<NavMeshAgent>();
        
        animator = GetComponent<Animator>();
        isWalkingHash = Animator.StringToHash("isWalk");
        _isAttackHash = Animator.StringToHash("isAttack2");
        
    }

    void Update ()
    {
        bool isWalking = animator.GetBool(isWalkingHash);
        
        // Get the distance to the player
        float distance = Vector3.Distance(target.position, transform.position);

        // If inside the radius
        if (distance <= lookRadius)
        {
            // Move towards the player
            agent.SetDestination(target.position);
            animator.SetBool(isWalkingHash,true);
            
            if (distance <= agent.stoppingDistance)
            {
                // Idle
                animator.SetBool(isWalkingHash,false);
                FaceTarget();
                
                /*if (Attacked == false)
                {
                    Attack();
                    /*animator.SetBool(_isAttackHash,false);#1#

                }*/
                /*if (Time.time >= nextAttackTime)
                {
                    Attacked = false;
                    if (Attacked == false)
                    {
                        Attack();
                        animator.SetBool(_isAttackHash,false);
                        nextAttackTime = Time.time + 3f / attactRate;
                        
                    }

                }*/
                
            }
            /*if (distance <= attackRadius)
            {
                if (Time.time >= nextAttackTime)
                {
                    if (Attacked == false)
                    {
                        Attack();
                        nextAttackTime = Time.time + 3f / attactRate;
                    }
                    /*Invoke("cancleAttack",2f);
                    Invoke("Attackagin",3f);#1#
                }
                /*Attack();#1#
                /*Invoke("Attack",0.5f);#1#
            }*/
        }

        if (distance > lookRadius)
        {
            Waypoint();
        }
    }
    /*public void Attack()
    {
        /*animator.SetTrigger(_isAttackHash);#1#
        animator.SetBool(_isAttackHash,true);
        Collider[] hitPlayer = Physics.OverlapSphere(attackPoint.position, attackRange, playerLayers);

        foreach (Collider player in hitPlayer)
        {
            player.GetComponent<HealthManager>().DamagePlayer(attackDamage);
        }

        Attacked = true;
    } */  
    
    /*public void Attackagin()
    {
        Attacked = false;
    }

    public void cancleAttack()
    {
        Attacked = true;
    }*/
    
    public void Waypoint()
    {
        animator.SetBool(isWalkingHash,true);
        if (waypoint.Count == 0)
        {
            return;
        }

        float distranceTowaypoint = Vector3.Distance(waypoint[currentWaypointIndex].position, transform.position);
        if (distranceTowaypoint <= 3)
        {
            currentWaypointIndex = (currentWaypointIndex + 1) % waypoint.Count;
        }

        agent.SetDestination(waypoint[currentWaypointIndex].position);
    }
    
    // Point towards the player
    void FaceTarget ()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);

    }

    void OnDrawGizmosSelected ()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
        /*Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);*/
    }
}
