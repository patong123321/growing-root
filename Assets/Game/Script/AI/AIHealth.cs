using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class AIHealth : MonoBehaviour
{
    public float aiHealth;
    public float aiMaxHealth = 100;

    //HealthBar
    public Slider healthBar;

    public bool death;
    
    //Animate
    Animator animator;
    int isDeathgHash;
    NavMeshAgent agent;
    int _isDeath;
    int _isHurt;
    public static bool hurt;
    


    void Start()
    {
        aiHealth = aiMaxHealth;
        animator = GetComponent<Animator>();
        /*isDeathgHash = Animator.StringToHash("isDeath");*/
        _isHurt = Animator.StringToHash("isHurt");
        _isDeath = Animator.StringToHash("isDeath");
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (healthBar.value != aiHealth)
        {
            healthBar.value = aiHealth;
        }
        
        if (healthBar.value == 0)
        {
            healthBar.value = 0;
            Die();
            
        }
        
        if (Input.GetKeyDown(KeyCode.Z))
        {
            TakeDamageAI(10);
        }
        
    }

    public void TakeDamageAI(float damage)
    {
        healthBar.value = aiHealth;
        aiHealth-= damage;
        animator.SetTrigger(_isHurt);
        
        
        /*if (aiHealth <= 0)
        {
            Die();
        }*/

    }
    
    public void Die()
    {
        Debug.Log("Die");
        animator.SetBool(_isDeath,true);
        GetComponent<Collider>().enabled = false;
        this.enabled = false;
        Invoke("DestroyAI",10f);
        agent.speed = 0f;
    }

    public void Attackagain()
    {
        hurt = false;
    }

    public void DestroyAI()
    {
        Destroy(gameObject);
    }

    

}   
