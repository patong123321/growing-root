using System.Collections;
using System.Collections.Generic;
using StarterAssets;
using UnityEngine;

public class GOD : MonoBehaviour
{
    public GameObject desert;

    public GameObject winter;

    public GameObject item;
    
    public GameObject essence;

    public GameObject Skill1;
    public GameObject Skill2;
    public GameObject Skill3;
    public GameObject Skill4;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Desert
        if (Input.GetKeyDown(KeyCode.F1))
        {
            desert.SetActive(true);
        }
        //Snow
        if (Input.GetKeyDown(KeyCode.F2))
        {
            winter.SetActive(true);
        }
        // Skill
        if (Input.GetKeyDown(KeyCode.F3))
        {
            ThirdPersonController.GetHighJump = true;
            ThirdPersonController.GetDash = true;
            ThirdPersonController.GetDoubleJump = true;
            Skill1.SetActive(true);
            Skill2.SetActive(true);
            Skill3.SetActive(true);
            Skill4.SetActive(true);
        }
        //Plant
        if (Input.GetKeyDown(KeyCode.F4))
        {
            item.SetActive(true);
        }
        //Essence
        if (Input.GetKeyDown(KeyCode.F5))
        {
            essence.SetActive(true);
        }
    }
}
