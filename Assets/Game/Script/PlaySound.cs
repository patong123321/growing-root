using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaySoundWall()
    {
        SoundManager.instance.wallSource.PlayOneShot(SoundManager.instance.wallSound);
    }
    
    public void PlaySoundRotate()
    {
        SoundManager.instance.rotateSource.PlayOneShot(SoundManager.instance.rotateSound);
    }
}
