using System.Collections;
using System.Collections.Generic;
using StarterAssets;
using UnityEngine;

public class Lock : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            /*Cursor.lockState = CursorLockMode.None;*/
            StarterAssetsInputs.cursorInputForLook = false;
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            StarterAssetsInputs.cursorInputForLook = true;
            /*Cursor.lockState = CursorLockMode.Locked;*/
        }
    }
}
