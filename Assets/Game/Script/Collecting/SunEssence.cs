using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SunEssence : MonoBehaviour
{
    public int SunES_Value;
    
    public GameObject effect;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            FindObjectOfType<Gamemanager>().AddSunEssence(SunES_Value);
            SoundManager.instance.SunEssenceSource.PlayOneShot(SoundManager.instance.SunEssencesound);
            Instantiate(effect, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
    
    public void Playingsound()
    {
        Invoke("Playingsound",0f);
        Debug.Log("play");
    }
}
