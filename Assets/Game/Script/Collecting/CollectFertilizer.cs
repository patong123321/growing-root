using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectFertilizer : MonoBehaviour
{
    public static bool GetFertilizer;
    public GameObject portal;
    // Start is called before the first frame update
    void Start()
    {
        GetFertilizer = false;
        portal.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            SoundManager.instance.EssenceSource.PlayOneShot(SoundManager.instance.Essencesound);
            GetFertilizer = true;
            portal.SetActive(true);
            Destroy(gameObject);
        }
    }
}
