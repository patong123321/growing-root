using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Essence : MonoBehaviour
{
    public int Es_Value;

    public GameObject effect;

    public bool playSound;

    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            FindObjectOfType<Gamemanager>().AddEssence(Es_Value);
            Destroy(gameObject);
            
            SoundManager.instance.EssenceSource.PlayOneShot(SoundManager.instance.Essencesound);

            Instantiate(effect, transform.position, transform.rotation);
        }

    }

    public void Playingsound()
    {
        Invoke("Playingsound",0f);
        Debug.Log("play");
    }
}
