using System.Collections;
using System.Collections.Generic;
using HealthSystem;
using UnityEngine;
using UnityEngine.Serialization;

public class PlantTree : MonoBehaviour
{
    public GameObject Seed;
    public GameObject plant;

    public GameObject Water;

    public GameObject Fertilizer;

    public bool State_Seed;
    [FormerlySerializedAs("State_Sap")] public bool State_Tree;
    [FormerlySerializedAs("State_Tree")] public bool State_BigTree;

    public GameObject dessert_Portal;
    public GameObject winter_Portal;
    // Start is called before the first frame update
    void Start()
    {
        dessert_Portal.SetActive(false);
        winter_Portal.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void plantSeed()
    {
        if (CollectSeed.GetSeed == true)
        {
            Seed.SetActive(true);
            CollectSeed.GetSeed = false;
            State_Seed = true;
            QuestGiver.completeSeed = true;
            dessert_Portal.SetActive(true);
            plant.SetActive(false);
        }

        if (CollectWater.GetWater == true && State_Seed == true)
        {
            Seed.SetActive(false);
            Water.SetActive(true);
            CollectWater.GetWater = false;
            State_Tree = true;
            QuestGiver.completeWater = true;
            winter_Portal.SetActive(true);
        }
        
        if (CollectFertilizer.GetFertilizer == true && State_Tree == true )
        {
            Seed.SetActive(false);
            Water.SetActive(false);
            Fertilizer.SetActive(true);
            CollectFertilizer.GetFertilizer = false;
            State_BigTree = true;
            QuestGiver.completeFertilizer = true;
        }
    }
}
