using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectWater : MonoBehaviour
{
    public static bool GetWater;
    public bool onPortal;
    public GameObject portal;
    // Start is called before the first frame update
    void Start()
    {
        GetWater = false;
        portal.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            SoundManager.instance.EssenceSource.PlayOneShot(SoundManager.instance.Essencesound);
            GetWater = true;
            portal.SetActive(true);
            
            Destroy(gameObject);

        }
    }

    public void setportal()
    {
        
    }
}
