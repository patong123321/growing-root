using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectSeed : MonoBehaviour
{
    public static bool GetSeed;

    public GameObject portal; 
    public GameObject plant;
    // Start is called before the first frame update
    void Start()
    {
        GetSeed = false;
        portal.SetActive(false);
        plant.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            SoundManager.instance.EssenceSource.PlayOneShot(SoundManager.instance.Essencesound);
            GetSeed = true;
            portal.SetActive(true);
            plant.SetActive(true);
            Destroy(gameObject);
        }
    }
}
