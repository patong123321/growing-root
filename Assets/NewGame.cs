using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewGame : MonoBehaviour
{
    public GameObject newGame;
    // Start is called before the first frame update
    void Start()
    {
        QuestGiver.completeFertilizer = false;
        newGame.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (QuestGiver.completeFertilizer == true)
        {
            newGame.SetActive(true);
            if (Input.GetKeyDown(KeyCode.F))
            {  
                SceneManager.LoadScene("MainMenu");
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }
       
        
    }
}
